/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task07;

import task02.View;
import task02.Viewable;

/**
 * ConcreteCreator (шаблон проектирования Factory Method); реализует метод,
 * "фабрикующий" объекты
 *
 * @author Ser
 * @version 1.0
 * @see Viewable
 * @see Viewable#getView()
 */
public class ViewableWindow implements Viewable {

    @Override
    public View getView() {
        return new ViewWindow();
    }
}
