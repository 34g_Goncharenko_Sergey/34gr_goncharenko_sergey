/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task06;

import java.util.HashSet;
import java.util.Set;

public abstract class Observable {

    private Set<Observer> observers = new HashSet<Observer>();

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void delObserver(Observer observer) {
        observers.remove(observer);
    }

    public void call(Object event) {
        for (Observer observer : observers) {
            observer.handleEvent(this, event);
        }
    }
}
