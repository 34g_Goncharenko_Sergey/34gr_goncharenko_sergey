/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task06;

public class ItemsGenerator extends AnnotatedObserver {

    @Event(Items.ITEMS_EMPTY)
    public void itemsEmpty(Items observable) {
        for (Item item : observable) {
            if (item.getData().isEmpty()) {
                int len = (int) (Math.random() * 10) + 1;
                String data = "";
                for (int n = 1; n <= len; n++) {
                    data += (char) ((int) (Math.random() * 26) + 'A');
                }
                item.setData(data);
            }
        }
        observable.call(Items.ITEMS_CHANGED);
    }
}
