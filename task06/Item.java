/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task06;

/**
 *
 * @author Ser
 */
public class Item implements Comparable<Item> {

    private String data;

    public Item(String data) {
        this.data = data;
    }

    public String setData(String data) {
        return this.data = data;
    }

    public String getData() {
        return data;
    }

    @Override
    public int compareTo(Item o) {
        return data.compareTo(o.data);
    }

    @Override
    public String toString() {
        return data;
    }
}
