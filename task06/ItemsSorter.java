/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task06;

import java.util.Collections;

public class ItemsSorter extends AnnotatedObserver {

    public static final String ITEMS_SORTED = "ITEMS_SORTED";

    @Event(Items.ITEMS_CHANGED)
    public void itemsChanged(Items observable) {
        Collections.sort(observable.getItems());
        observable.call(ITEMS_SORTED);
    }

    @Event(ITEMS_SORTED)
    public void itemsSorted(Items observable) {
        System.out.println(observable.getItems());
    }

    @Event(Items.ITEMS_REMOVED)
    public void itemsRemoved(Items observable) {
        System.out.println(observable.getItems());
    }
}
