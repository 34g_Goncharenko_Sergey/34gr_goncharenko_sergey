/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task01;

/**
 *
 * @author Ser
 */
import java.io.Serializable;

/**
 * Хранит исходные данные и результат вычислений.
 *
 * @author xone
 * @version 1.0
 */
public class Item2d implements Serializable {

    /**
     * Аргумент вычисляемой функции.
     */
// transient
    private double g;

    /**
     * Результат вычисления функции.
     */
    private double m;
    private double h;
    private double E;
    private String binaryE;
    private int sequential;
    
    /**
     * Автоматически сгенерированная константа
     */
    private static final long serialVersionUID = 1L;

    /**
     * Инициализирует поля
     * {@link Item2d#g}, {@link Item2d#m}, {@link Item2d#h}, {@link Item2d#result}, {@link Item2d#binaryE}
     */
    public Item2d() {
        g = 9.8;
        m = .0;
        h = .0;
        E = .0;
        binaryE = "";
        sequential = 0;
    }

    /**
     * Устанавливает значения полей: аргумента и результата вычисления функции.
     *
     * @param g - значение для инициализации поля {@link Item2d#g}
     * @param m - значение для инициализации поля {@link Item2d#m}
     * @param h - значение для инициализации поля {@link Item2d#h}
     * @param E - значение для инициализации поля {@link Item2d#E}
     * @param binaryE - значение для инициализации поля {@link Item2d#binaryE}
     * @param sequential - значение для инициализации поля {@link Item2d#sequential}
     */
    public Item2d(double g, double m, double h, double E, String binaryE, int sequential) {
        this.g = g;
        this.m = m;
        this.h = h;
        this.E = E;
        this.binaryE = binaryE;
        this.sequential = sequential;
    }

    /**
     * Установка значения поля {@link Item2d#g}
     *
     * @param g - значение для {@link Item2d#g}
     * @return Значение {@link Item2d#g}
     */
    public double setG(double g) {
        return this.g = g;
    }

    /**
     * Получение значения поля {@link Item2d#g}
     *
     * @return Значение {@link Item2d#g}
     */
    public double getG() {
        return g;
    }

    /**
     * Установка значения поля {@link Item2d#m}
     *
     * @param Octa - значение для {@link Item2d#m}
     * @return Значение {@link Item2d#m}
     */
    public double setM(double m) {
        return this.m = m;
    }

    /**
     * Получение значения поля {@link Item2d#m}
     *
     * @return значение {@link Item2d#m}
     */
    public double getM() {
        return m;
    }

    /**
     * Установка значения поля {@link Item2d#h}
     *
     * @param h - значение для {@link Item2d#h}
     * @return Значение {@link Item2d#h}
     */
    public double setH(double h) {
        return this.h = h;
    }

    /**
     * Получение значения поля {@link Item2d#h}
     *
     * @return значение {@link Item2d#h}
     */
    public double getH() {
        return h;
    }

    /**
     * Установка значения поля {@link Item2d#h}
     *
     * @param E - значение для {@link Item2d#result}
     * @return Значение {@link Item2d#h}
     */
    public double setE(double E) {
        return this.E = E;
    }

    /**
     * Получение значения поля {@link Item2d#h}
     *
     * @return значение {@link Item2d#h}
     */
    public double getE() {
        return E;
    }
    
    /**
     * Установка значения поля {@link Item2d#h}
     *
     * @param binaryE - значение для {@link Item2d#result}
     * @return Значение {@link Item2d#h}
     */
    public String setBinaryE(String binaryE) {
        return this.binaryE = binaryE;
    }

    /**
     * Получение значения поля {@link Item2d#h}
     *
     * @return значение {@link Item2d#h}
     */
    public String getBinaryE() {
        return binaryE;
    }
    /**
     * Установка значения поля {@link Item2d#h}
     *
     * @param binaryE - значение для {@link Item2d#result}
     * @return Значение {@link Item2d#h}
     */
    public int setSequential(int sequential) {
        return this.sequential = sequential;
    }

    /**
     * Получение значения поля {@link Item2d#h}
     *
     * @return значение {@link Item2d#h}
     */
    public int getSequential() {
        return sequential;
    }    
    
    /**
     * Установка значений {@link Item2d#g} и {@link Item2d#m}
     *
     * @param g - значение для {@link Item2d#g}
     * @param m - значение для {@link Item2d#m}
     * @param h - значение для {@link Item2d#h}
     * @param E - значение для {@link Item2d#E}
     * @param binaryE - значение для {@link Item2d#binaryE}
     * @param sequential - значение для {@link Item2d#sequential}
     * @return this
     */
    public Item2d setGMHResultBinaryE(double g, double m, double h, double E, String binaryE, int sequential) {
        this.g = g;
        this.m = m;
        this.h = h;
        this.E = E;
        this.binaryE = binaryE;
        this.sequential = sequential;
        return this;
    }
    /**
     * Представляет результат вычислений в виде строки.<br>{@inheritDoc}
     */
    @Override
    public String toString() {
        return "\ng = " + g + ", m = " + m + ", h = " + h + ", E = " + E + "\nBinary view of E = " + binaryE + "\nMax count of sequential 1: " + sequential;
    }
}

