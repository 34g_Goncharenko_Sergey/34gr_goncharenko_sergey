/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task01;

/**
 *
 * @author Ser
 */
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Содержит реализацию методов для вычисления и отображения результатов.
 *
 * @author xone
 * @version 1.0
 */
public class Calc {

    /**
     * Имя файла, используемое при сериализации.
     */
    private static final String FNAME = "Item2d.bin";

    /**
     * Сохраняет результат вычислений. Объект класса {@linkplain Item2d}
     */
    private Item2d result;

    /**
     * Инициализирует {@linkplain Calc#result}
     */
    public Calc() {
        result = new Item2d();
    }

    /**
     * Установить значение {@linkplain Calc#result}
     *
     * @param result - новое значение ссылки на объект {@linkplain Item2d}
     */
    public void setResult(Item2d result) {
        this.result = result;
    }

    /**
     * Получить значение {@linkplain Calc#result}
     *
     * @return текущее значение ссылки на объект {@linkplain Item2d}
     */
    public Item2d getResult() {
        return result;
    }

    /**
     * Вычисляет значение функции.
     *
     * @param m - аргумент вычисляемой функции.
     * @param h - аргумент вычисляемой функции.
     * @param g - аргумент вычисляемой функции.
     * @param q - аргумент вычисляемой функции.
     * @param E - аргумент вычисляемой функции.
     * @param binE - аргумент вычисляемой функции.
     * @param sequential - аргумент вычисляемой функции.
     * @param maxSequential - результат вычислений.
     * @return результат вычисления функции.
     */
    private void calc(double g, double m, double h) {
       int sequential = 0;
       int maxSequential = 0;
       double E = m*g*h;
       long q = (long) E;
       String binaryE = Long.toBinaryString(q);
       result.setE(E);
       result.setBinaryE(binaryE);
       char[] binE = binaryE.toCharArray();

       for(int i = 0; i < binE.length;i++){
            if(binaryE.charAt(i) == '1'){
                sequential ++;
            }else if(sequential > maxSequential){
                      maxSequential = sequential;
                      sequential = 0;
                  }else {
                sequential = 0;
            }
       }
       
        result.setSequential(maxSequential);
    }

    /**
     * Вычисляет значение функции и сохраняет результат в объекте
     * {@linkplain Calc#result}
     *
     * @param g - аргумент вычисляемой функции.
     */
        public void init(double m, double h) {
        result.setM(m);
        result.setH(h);
        calc(9.8, m, h);
    }

    /**
     * Выводит результат вычислений.
     */
    public void show() {
        System.out.println(result);
    }

    /**
     * Сохраняет {@linkplain Calc#result} в файле {@linkplain Calc#FNAME}
     *
     * @throws IOException
     */
    public void save() throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
        os.writeObject(result);
        os.flush();
        os.close();
    }

    /**
     * Восстанавливает {@linkplain Calc#result} из файла {@linkplain Calc#FNAME}
     *
     * @throws Exception
     */
    public void restore() throws Exception {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
        result = (Item2d) is.readObject();
        is.close();
    }
}
