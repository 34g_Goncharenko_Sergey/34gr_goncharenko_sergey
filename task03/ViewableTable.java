/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task03;

/**
 *
 * @author Ser
 */

import task02.ViewableResult;
import task02.View;
/**
 * ConcreteCreator (шаблон проектирования Factory Method)<br>
 * Объявляет метод, "фабрикующий" объекты
 *
 * @author Ser
 * @version 1.0
 * @see ViewableResult
 * @see ViewableTable#getView()
 */
public class ViewableTable extends ViewableResult {

    /**
     * Создаёт отображаемый объект {@linkplain ViewTable}
     */
    @Override
    public View getView() {
        return new ViewTable();
    }
}