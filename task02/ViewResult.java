/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package task02;

/**
 *
 * @author Ser
 */

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import task01.Item2d;

/**
 * ConcreteProduct (Шаблон проектирования Factory Method)<br>
 * Вычисление функции, сохранение и отображение результатов
 *
 * @author Ser
 * @version 1.0
 * @see View
 */
public class ViewResult implements View {
    Scanner reader = new Scanner(System.in);

    /**
     * Имя файла, используемое при сериализации
     */
    private static final String FNAME = "items.bin";
    /**
     * Определяет количество значений для вычисления по умолчанию
     */
    private static final int DEFAULT_NUM = 10;
    /**
     * Коллекция аргументов и результатов вычислений
     */
    private ArrayList<Item2d> items = new ArrayList<Item2d>();

    /**
     * Вызывает {@linkplain ViewResult#ViewResult(int n) ViewResult(int n)} с
     * параметром {@linkplain ViewResult#DEFAULT_NUM DEFAULT_NUM}
     */
    public ViewResult() {
        this(DEFAULT_NUM);
    }

    /**
     * Инициализирует коллекцию {@linkplain ViewResult#items}
     *
     * @param n начальное количество элементов
     */
    public ViewResult(int n) {
        for (int ctr = 0; ctr < n; ctr++) {
            items.add(new Item2d());
        }
    }

    /**
     * Получить значение {@linkplain ViewResult#items}
     *
     * @return текущее значение ссылки на объект {@linkplain ArrayList}
     */
    public ArrayList<Item2d> getItems() {
        return items;
    }

    /**
     * Вычисляет значение функции 11
     *
     * @param m - аргумент вычисляемой функции.
     * @param h - аргумент вычисляемой функции.
     * @param g - аргумент вычисляемой функции.
     * @param q - аргумент вычисляемой функции.
     * @param E - аргумент вычисляемой функции.
     * @param binE - аргумент вычисляемой функции.
     * @param sequential - аргумент вычисляемой функции.
     * @param maxSequential - результат вычислений.
     * @return результат вычисления функции.
     */
    private void calc(double g,double m,double h, Item2d item) {
        
       int sequential = 0;
       int maxSequential = 0;
       double E = m*g*h;
       long q = (long) E;
       String binaryE = Long.toBinaryString(q);
       char[] binE = binaryE.toCharArray();

       for(int i = 0; i < binE.length;i++){
            if(binaryE.charAt(i) == '1'){
                sequential ++;
            }else if(sequential > maxSequential){
                      maxSequential = sequential;
                      sequential = 0;
                  }else {
                sequential = 0;
            }
       }
        
       item.setGMHResultBinaryE(g, m, h, E, binaryE, maxSequential);
    }

    /**
     * Вычисляет значение функции и сохраняет результат в коллекции
     * {@linkplain ViewResult#items}
     *
     * @param stepX шаг приращения аргумента
     */
    public void init(double m, double h, double stepX) {
        double g = 9.8;
        for (Item2d item : items) {
            item.setG(g);
            item.setM(m);
            item.setH(h);
            calc(g,m,h, item);
           m += stepX;
           h += stepX;
        }
    }

    /**
     * Вызывает <b>init(double stepX)</b> со случайным значением аргумента<br>
     * {@inheritDoc}
     */
    @Override
    public void viewInit() {
        init(reader.nextDouble(),reader.nextDouble(),reader.nextDouble());
    }

    /**
     * Реализация метода {@linkplain View#viewSave()}<br> {@inheritDoc}
     */
    @Override
    public void viewSave() throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(FNAME));
        os.writeObject(items);
        os.flush();
        os.close();
    }

    /**
     * Реализация метода {@linkplain View#viewRestore()}<br> {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void viewRestore() throws Exception {
        ObjectInputStream is = new ObjectInputStream(new FileInputStream(FNAME));
        items = (ArrayList<Item2d>) is.readObject();
        is.close();
    }

    /**
     * Реализация метода {@linkplain View#viewHeader()}<br> {@inheritDoc}
     */
    @Override
    public void viewHeader() {
        System.out.println("Results:");
    }

    /**
     * Реализация метода {@linkplain View#viewBody()}<br> {@inheritDoc}
     */
    @Override
    public void viewBody() {
        for (Item2d item : items) {
            System.out.printf("g = " + item.getG() + ", m = " + item.getM() + ", h = " + item.getH() + ", E = " + item.getE() + "\nBinary view of E = " + item.getBinaryE() + "\nMax count of sequential 1: " + item.getSequential() + "\n");
        }
        System.out.println();
    }

    /**
     * Реализация метода {@linkplain View#viewFooter()}<br> {@inheritDoc}
     */
    @Override
    public void viewFooter() {
        System.out.println("End.");
    }

    /**
     * Реализация метода {@linkplain View#viewShow()}<br> {@inheritDoc}
     */
    @Override
    public void viewShow() {
        viewHeader();
        viewBody();
        viewFooter();
    }
}
